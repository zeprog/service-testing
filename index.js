function testService(events) {
  if (!events.length) return true
  const customer = {}
  events.forEach(el => {
    const [name, type] = el
    if(!customer[name]) {
      if(type !== 'in') return false
      customer[name] = 1
    }
    if(type === 'in' && customer[name] % 2 !== 0) return false
    customer[name] += 1
  });

  for(let person in customer) {
    if(customer[person] % 2 !== 0) return false
  }
  return true
}

module.exports = testService
